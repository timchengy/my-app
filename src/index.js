var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();
const port = 3000


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  return res.json({ status: 1, version: process.env.npm_package_version, ctime: new Date() })
});

app.get('/api', (req, res) => {
  return res.json({ status: 1, version: process.env.npm_package_version, ctime: new Date() })
});

app.get('/user', (req, res) => {
  return res.json({ status: 1, user: 'Guest' })
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

module.exports = app;