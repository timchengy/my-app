FROM node:14-alpine

# Create app directory
WORKDIR /home/io.company.my-app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
RUN npm ci

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

ENV PORT 3000
EXPOSE $PORT
CMD [ "npm", "run", "dev" ]
