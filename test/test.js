const request = require('supertest');
const app = require('../src');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/status/, done);
  });
}); 
